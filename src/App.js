import GameMainPage from './components/GameMainPage'
import './App.css';

function App() {
  return (
    <div className="App">
      <GameMainPage />
    </div>
  );
}

export default App;
