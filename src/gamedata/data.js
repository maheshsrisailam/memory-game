export const gamedata = [
    {
        id:1,
        imageId: "dhoni",
        isMatched: false,
        imageUrl: "https://c.ndtvimg.com/2020-06/vfc2pfa_ms-dhoni-afp_625x300_19_June_20.jpg",

    },
    {
        id:2,
        imageId: "dhoni",
        isMatched: false,
        imageUrl: "https://c.ndtvimg.com/2020-06/vfc2pfa_ms-dhoni-afp_625x300_19_June_20.jpg",

    },
    {
        id:3,
        imageId: "animal",
        isMatched: false,
        imageUrl: "https://cdn.mos.cms.futurecdn.net/FVqUjfbiHS9imyJiRiM53-1200-80.jpg",

    },
    {
        id:4,
        imageId: "animal",
        isMatched: false,
        imageUrl: "https://cdn.mos.cms.futurecdn.net/FVqUjfbiHS9imyJiRiM53-1200-80.jpg",

    },
    {
        id:5,
        imageId: "house",
        isMatched: false,
        imageUrl: "https://camellacavitehomes.com/wp-content/uploads/2021/04/house-dani-thumbnail.jpg",

    },
    {
        id:6,
        imageId: "house",
        isMatched: false,
        imageUrl: "https://camellacavitehomes.com/wp-content/uploads/2021/04/house-dani-thumbnail.jpg",

    },
    {
        id:7,
        imageId: "fruits",
        isMatched: false,
        imageUrl: "https://images.news18.com/ibnlive/uploads/2022/01/fresh-fruits.jpg",

    },
    {
        id:8,
        imageId: "fruits",
        isMatched: false,
        imageUrl: "https://images.news18.com/ibnlive/uploads/2022/01/fresh-fruits.jpg",

    },
    {
        id:9,
        imageId: "car",
        isMatched: false,
        imageUrl: "https://www.autocar.co.uk/sites/autocar.co.uk/files/range-rover-2022-001-tracking-front.jpg",

    },
    {
        id:10,
        imageId: "car",
        isMatched: false,
        imageUrl: "https://www.autocar.co.uk/sites/autocar.co.uk/files/range-rover-2022-001-tracking-front.jpg",

    },
    {
        id:11,
        imageId: "bike",
        isMatched: false,
        imageUrl: "https://bd.gaadicdn.com/processedimages/yamaha/mt-15-2-0/640X309/mt-15-2-062e4b1493f453.jpg",

    },
    {
        id:12,
        imageId:'bike',
        isMatched: false,
        imageUrl: "https://bd.gaadicdn.com/processedimages/yamaha/mt-15-2-0/640X309/mt-15-2-062e4b1493f453.jpg",

    },
    {
        id:13,
        isMatched: false,
        imageId: 'ship',
        imageUrl: "https://cruiseindustrynews.com/wp-content/uploads/2020/12/world_europa1.jpg",

    },
    {
        id:14,
        imageId:'ship',
        isMatched: false,
        imageUrl: "https://cruiseindustrynews.com/wp-content/uploads/2020/12/world_europa1.jpg",

    },
    {
        id:15,
        imageId:'river',
        isMatched: false,
        imageUrl: "https://www.macmillandictionary.com/external/slideshow/full/152264_full.jpg",

    },
    {
        id:16,
        imageId:'river',
        isMatched: false,
        imageUrl: "https://www.macmillandictionary.com/external/slideshow/full/152264_full.jpg",

    },

]