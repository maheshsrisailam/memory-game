import './gameimage.css'
const GameImageCard = (props) => {
    const {imageDetails,clickedImage,flipped,disabled} = props
    const {imageUrl,imageId} = imageDetails

    const handleClick = () => {
        if (!disabled) {
            clickedImage(imageDetails)
        }
    }
    return (
        <div className='card'>
            <div onClick={handleClick} className={flipped ? "flipped" : ""}>
                <img src={imageUrl} alt={imageId} className="front" />
                <img src="https://wallpaperaccess.com/full/187161.jpg" className='back' alt="back" />
            </div>
        </div>
    )
}

export default GameImageCard;