import {useEffect, useState} from 'react'

import { gamedata } from '../../gamedata/data'
import GameImageCard from '../GameImageCard'

import './index.css'


const imagesData = [...gamedata].sort(() => Math.random()-0.5).map(image=>({...image,isMatched:false}))
const GameMainPage = () => {
    const [start, setStart] = useState(false)
    const [imagesArray, setImagesArray] = useState(imagesData)
    const [firstClickedImage, setfirstClickedImage] = useState(null)
    const [secondClickedImage, setsecondClickedImage] = useState(null)
    const [attempts, setattempts] = useState(0)
    const [disabled,setDisabled] = useState(false)
    const [counter, setCounter] = useState(0)
    const [gameWon,setGameWon] = useState(false)

    const shaffleTheImages = () => {
        const shaffledImages = [...imagesArray].sort(() => Math.random()-0.5).map(image=>({...image, isMatched:false}))
        setImagesArray(shaffledImages)
        setattempts(0)
        setfirstClickedImage(null)
        setsecondClickedImage(null)
        setCounter(0)
        setGameWon(false)
    }

    let minutes = Math.floor(counter / 60);
    let seconds = counter - minutes * 60

    const clickedImage = (imageDetails) => {
        firstClickedImage ? setsecondClickedImage(imageDetails): setfirstClickedImage(imageDetails)
        checkGameStatus()
    }
    
    useEffect(()=>{
        if (firstClickedImage && secondClickedImage) {
            setDisabled(true)
            if (firstClickedImage.imageUrl === secondClickedImage.imageUrl) {
                setImagesArray(prevImagesArray=> {
                    return prevImagesArray.map((image) => {
                        if (image.imageId ===firstClickedImage.imageId) {
                            return {...image, isMatched: true}
                        } else {
                            return image
                        }
                    })
                })
                resetTurn()
            }else {
                setTimeout(()=>resetTurn(),1000)
            }
        }

    },[firstClickedImage,secondClickedImage])

    useEffect(()=>{
        shaffleTheImages()
        let intervalId = null
        if (start) {
            intervalId = setInterval(()=>{
                setCounter(prevCount=>prevCount+1)
            },1000)
        }

        if (gameWon) {
            setGameWon(true)
            setCounter(counter)
            setattempts(attempts)
            clearInterval(intervalId)
        }
        return () => clearInterval(intervalId)
    },[start,gameWon])

    function resetTurn() {
        setfirstClickedImage(null)
        setsecondClickedImage(null)
        setattempts(prev=>prev+1)
        setDisabled(false)
    }

    const handleStart = () => {
        setStart(true)
    }

    const renderStartDisplayView = () => (
        <div className='start-display-view'>
            {renderDescriptionSection()}
            <button onClick={handleStart} className="start-button">Start</button>
        </div>
    ) 

    const checkGameStatus = ()=>{
        const allImagesMatched = imagesArray.every((image)=>image.isMatched===true)
        console.log(allImagesMatched)
        if (allImagesMatched) {
            setGameWon(true)
        }
    }

    const textStyle = start ? "" : "text-style"
    const renderDescriptionSection = () => (
        <>
            <h1 className={`memory-game-heading ${textStyle}`}>Memory Game</h1>
            <div className='counter-attempts-display'>
                <p className={`counter ${textStyle}`}>{attempts} Move(s)</p>
                <p className={`counter ${textStyle}`}>{minutes} mins {seconds} secs</p>
                <button className='reset' onClick={shaffleTheImages}>
                    <img src="https://png.pngtree.com/element_our/20190601/ourlarge/pngtree-white-refresh-icon-image_1338657.jpg" className='reset-image' alt="reset" />
                </button>
            </div>
        </>
    )

    const renderGameDisplayView = () => (
        <div className="main-container">
            {renderDescriptionSection()}
            <div className='card-grid'>
                {imagesArray.map((image) => (
                    <GameImageCard 
                        key={image.id} 
                        imageDetails={image} 
                        clickedImage={clickedImage} 
                        flipped={image === firstClickedImage || image === secondClickedImage || image.isMatched}
                        disabled={disabled}
                    />
                ))}
            </div>
        </div>
    )

    const renderShowWinningView = () => (
        <div className='winning-card'>
            <h1 className='you-win'>You Win!!</h1>
            <p className='win-text'>Total attempts: {attempts}</p>
            <p className='win-text'>Time Taken: {minutes} mins {seconds}secs</p>
            <button className='reset' onClick={shaffleTheImages}>
                <img src="https://png.pngtree.com/element_our/20190601/ourlarge/pngtree-white-refresh-icon-image_1338657.jpg" className='reset-image' alt="reset" />
            </button>
        </div>
    )

    return ( 
        <div>
            {!gameWon ? start ? renderGameDisplayView() : renderStartDisplayView(): null}
            {gameWon ? renderShowWinningView():null}
        </div>
     );
}
 
export default GameMainPage;
